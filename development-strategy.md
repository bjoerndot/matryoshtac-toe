# 1. Regular Tic Tac Toe

## The Field

- 9 button elements. As long as no player has clicked them, they are enabled. once it is clicked, they will be disabled
- State is stored in Array of arrays (3x3). A field that is already occupied by a player is marked with an object that holds `name` of player and `value` in this case 1 or 0
- Strategy for the computer:
  1. Defend = is the enemy about to close a line?
  2. try to complete a line
  3. capture the center
  4. Capture a corner
  5. random
- reverse starting, for rematches (stored in a single variable)
- add some "human" delay for the computer action

# 2. Russion Tic Tac Toe

- each player has its 9 figurines in front of him. Before clicking anything on the board the figurine must be clicked
- a figurine is a button, that is disabled once it is used (how to store?)
- the players line is on the bottom of the screen. for a11y reasons it should be first though -> change with flex
