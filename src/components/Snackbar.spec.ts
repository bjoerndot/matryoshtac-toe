import { describe, it, expect, beforeEach, vi, afterEach } from 'vitest';
import { snackbar } from '../stores/snackbar.store';
import Snackbar from './Snackbar.svelte';
import { tick } from 'svelte';

describe('Snackbar', () => {
	let instance: Snackbar;
	let host: HTMLElement;

	beforeEach(() => {
		host = document.createElement('div');
		document.body.appendChild(host);
		instance = new Snackbar({ target: host });
	});

	afterEach(() => {
		host.remove();
	});

	it('creates an instance', function () {
		expect(instance).toBeTruthy();
	});

	it('updates count when clicking a button', async function () {
		const spy = vi.spyOn(snackbar, 'hide');
		snackbar.show('test');
		await tick();
		const btn = host.getElementsByTagName('button')[0];

		btn?.click();
		await tick();

		expect(spy).toHaveBeenCalledOnce();
	});
});
