export const announce = (message: string) => {
	const liveRegion = document.querySelector('#svelte-announcer');
	if (liveRegion) {
		liveRegion.innerHTML = message;
	}
};

export function focusFirstButton(identifier: string) {
	const el: HTMLElement | null = document.querySelector(identifier);
	const buttons: HTMLButtonElement[] = Array.from(
		el?.querySelectorAll<HTMLButtonElement>('button') || []
	).filter((node) => node.nodeName === 'BUTTON' && !node.disabled);
	if (buttons.length > 0) {
		buttons[0].focus();
	}
}
