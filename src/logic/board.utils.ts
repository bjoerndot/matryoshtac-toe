import type { Board, ConditionAt, Field } from '../stores/board.store';

export const getAllVariations = (board: Board): Field[][][] => {
	const columns = getColumns(board);
	const across = getAcross(board);
	return [board, columns, across];
};

export const getColumns = (board: Board): Board =>
	board[0].map((_, i) => board.map((x) => x[i])) as Board;

export const getAcross = (board: Board): Field[][] => [
	[board[0][0], board[1][1], board[2][2]],
	[board[0][2], board[1][1], board[2][0]]
];

export const iterateOverVariations = (
	variations: Field[][][],
	callback: (a: Field[]) => string | undefined
): ConditionAt[] => {
	const matches = [];
	for (let i = 0; i < variations.length; i++) {
		let innerResult;
		const variation = variations[i];
		for (let j = 0; j < variation.length; j++) {
			innerResult = callback(variation[j]);
			if (innerResult) {
				console.log('innerResult: ', innerResult);
				matches.push({ name: innerResult, position: { inner: j, outer: i } });
			}
		}
	}
	return matches;
};

export const boardIsFull = (board: Board): boolean =>
	!board.some((row) => row.some((field) => field.name === ''));

export const boardIsEmpty = (board: Board): boolean =>
	board.every((row) => row.every((field) => field.name === ''));

function checkTriple(triple: Field[]): string {
	if (triple.map((t) => t.name).every((val, _, arr) => val === arr[0]) && triple[0]) {
		return triple[0].name;
	} else {
		return '';
	}
}

export const checkWinCondition = (board: Board): string => {
	const variations = getAllVariations(board);
	let name = '';
	const threeInARow = iterateOverVariations(variations, checkTriple);
	if (threeInARow.length > 0) {
		name = threeInARow[0].name;
	}
	return name;
};
