import {
	board,
	type Board,
	type BoardPosition,
	type ConditionAt,
	type ConditionPosition,
	type Field
} from '../stores/board.store';
import { PlayerType, puppets, type PuppetsType, type PuppetType } from '../stores/puppets.store';
import {
	boardIsFull,
	checkWinCondition,
	getAcross,
	getAllVariations,
	getColumns,
	iterateOverVariations
} from './board.utils';

function checkTwoInRow(row: Field[]): string | undefined {
	const counted = row
		.map((field) => field.name)
		.reduce((prev, curr) => {
			prev[curr] = (prev[curr] || 0) + 1;
			return prev;
		}, {} as { [key: string]: number });

	return Object.keys(counted).some((key) => key === '')
		? Object.entries(counted)
				.map(([key, value]) => (value > 1 ? key : undefined))
				.filter((key) => key !== '')
				.find((key) => key !== undefined)
		: undefined;
}

function areTwoInARow(board: Board): ConditionAt[] {
	const variations = getAllVariations(board);
	return iterateOverVariations(variations, checkTwoInRow);
}

function findFreeInRow(row: Field[]): number {
	return row.findIndex((field) => field.name === '');
}

function findFreeField(board: Board, position: ConditionPosition): BoardPosition {
	if (position.outer === 0) {
		// Match was in row
		const column = findFreeInRow(board[position.inner]);
		return { row: position.inner, column };
	} else if (position.outer === 1) {
		// Match was in column
		const row = findFreeInRow(getColumns(board)[position.inner]);
		return { row, column: position.inner };
	} else if (position.outer === 2) {
		const index = findFreeInRow(getAcross(board)[position.inner]);
		if (position.inner === 0) {
			return { row: index, column: index };
		} else {
			return { row: index, column: Math.abs(index - 2) };
		}
	}
	return { row: NaN, column: NaN };
}

function setAtField(position: BoardPosition, value: number) {
	board.setOn(position, PlayerType.computer, value);
	puppets.use(value, PlayerType.computer);
}

function isFree(board: Board, position: BoardPosition): boolean {
	return board[position.row][position.column].value === 0;
}

/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 */
function shuffle<T>(a: T[]): T[] {
	for (let i = a.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[a[i], a[j]] = [a[j], a[i]];
	}
	return a;
}

function getFreePuppets(puppets: PuppetsType): PuppetType[] {
	const freePuppets = puppets[PlayerType.computer].filter((puppet) => !puppet.inUse);
	if (freePuppets.length === 0) {
		throw new Error('Computer has used all its puppets. This should not be possible');
	}
	return freePuppets;
}

function getRandomPuppet(puppets: PuppetsType): number {
	const freePuppets = shuffle(getFreePuppets(puppets));
	return freePuppets[0].value;
}

function getHighestPuppet(puppets: PuppetsType): number {
	const freePuppets = getFreePuppets(puppets);
	return freePuppets[freePuppets.length - 1].value;
}

export function calculateNextMove(board: Board, puppets: PuppetsType) {
	if (boardIsFull(board) || checkWinCondition(board)) {
		return;
	}
	const value = getRandomPuppet(puppets);
	// 1. Defend - if player has two in a row, take the third.
	// 2. Attack - if computer has two in a row, take the third.
	const twoInARow = areTwoInARow(board)
		.sort((a, b) => (a.name > b.name ? 1 : -1))
		.filter((row) => row.name !== '');
	if (twoInARow.length > 0) {
		let finished = false;
		do {
			const fieldToTake = findFreeField(board, twoInARow[0].position);
			if (isFree(board, fieldToTake)) {
				setAtField(fieldToTake, getHighestPuppet(puppets));
				finished = true;
				break;
			}
		} while (twoInARow.length > 0);

		if (finished) {
			return;
		}
	}
	// 3. Conquer Center
	const center = { row: 1, column: 1 };
	if (isFree(board, center)) {
		setAtField(center, value);
		return;
	}
	// 4. Conquer Corner
	let corners: BoardPosition[] = [
		{ row: 0, column: 0 },
		{ row: 0, column: 2 },
		{ row: 2, column: 2 },
		{ row: 2, column: 0 }
	];
	corners = shuffle(corners);
	for (let i = 0; i < corners.length; i++) {
		const freeCorner = isFree(board, corners[i]);
		if (freeCorner) {
			setAtField(corners[i], value);
			return;
		}
	}
	// 5. Random
	const numbers = [0, 1, 2];
	let row: number;
	let column: number;

	do {
		row = numbers[Math.floor(Math.random() * numbers.length)];
		column = numbers[Math.floor(Math.random() * numbers.length)];
	} while (!isFree(board, { row, column }));

	setAtField({ row, column }, value);
	return;
}
