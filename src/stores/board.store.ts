import { derived, writable, type Readable } from 'svelte/store';
import { boardIsEmpty, boardIsFull, checkWinCondition } from '../logic/board.utils';
import { PlayerType } from './puppets.store';

export interface Field {
	name: PlayerType;
	value: number;
}

type Row = [Field, Field, Field];

export type Board = [Row, Row, Row];

export interface BoardPosition {
	row: number;
	column: number;
}

export interface FieldEvent {
	field: Field;
	position: BoardPosition;
}

export interface ConditionPosition {
	inner: number;
	outer: number;
}
export interface ConditionAt {
	name: string;
	position: ConditionPosition;
}

const emptyField: Field = { name: PlayerType.none, value: 0 };

const initialValueRow: Row = [{ ...emptyField }, { ...emptyField }, { ...emptyField }];
const initalValueBoard: Board = [[...initialValueRow], [...initialValueRow], [...initialValueRow]];

function createBoard() {
	const { subscribe, set, update } = writable(structuredClone(initalValueBoard));

	function updateBoardAt(at: BoardPosition, name: Field['name'], value: Field['value']) {
		update((board) => {
			board[at.row][at.column] = { name, value };
			return board;
		});
	}

	return {
		subscribe,
		reset: () => set(structuredClone(initalValueBoard)),
		setOn: updateBoardAt
	};
}

export const board = createBoard();

function checkForDraw(board: Board): string {
	return boardIsFull(board) ? 'draw' : '';
}

export const termination = derived(board, ($board) => {
	let name = checkWinCondition($board);
	// Check for draw
	if (!name) {
		name = checkForDraw($board);
	}
	return name === '' ? null : name;
});

export const empty: Readable<boolean> = derived(board, ($board) => boardIsEmpty($board));
