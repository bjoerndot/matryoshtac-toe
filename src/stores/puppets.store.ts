import { derived, writable } from 'svelte/store';

export interface PuppetType {
	value: number;
	inUse: boolean;
	active: boolean;
}

export interface PuppetsType {
	[PlayerType.computer]: PuppetType[];
	[PlayerType.player]: PuppetType[];
}

export enum PlayerType {
	player = 'player',
	computer = 'computer',
	none = ''
}

const initialPuppets: PuppetType[] = [1, 2, 3, 4, 5, 6, 7, 8, 9].map((value) => ({
	value,
	inUse: false,
	active: false
}));
const initialPuppetsPerPlayer: PuppetsType = {
	[PlayerType.computer]: structuredClone(initialPuppets),
	[PlayerType.player]: structuredClone(initialPuppets)
};

function createPuppets() {
	const { subscribe, set, update } = writable(structuredClone(initialPuppetsPerPlayer));

	const findPuppetIndex = (
		puppets: PuppetsType,
		player: PlayerType.computer | PlayerType.player,
		value: number
	): number => puppets[player].findIndex((puppet) => puppet.value === value);

	const resetActive = (puppets: PuppetsType, player: PlayerType.computer | PlayerType.player) =>
		puppets[player].forEach((puppet) => (puppet.active = false));

	const usePuppet = (value: number, player: PlayerType) => {
		if (player === PlayerType.none) {
			return;
		}
		update((puppets) => {
			resetActive(puppets as PuppetsType, player);
			const index = findPuppetIndex(puppets as PuppetsType, player, value);
			puppets[player][index].inUse = true;
			return puppets;
		});
	};

	const setPuppetActive = (value: number, player: PlayerType) => {
		if (player === PlayerType.none) {
			return;
		}
		update((puppets) => {
			resetActive(puppets as PuppetsType, player);
			const index = findPuppetIndex(puppets as PuppetsType, player, value);
			puppets[player][index].active = true;

			return puppets;
		});
	};

	return {
		subscribe,
		reset: () => set(structuredClone(initialPuppetsPerPlayer)),
		use: usePuppet,
		setActive: setPuppetActive
	};
}

export const puppets = createPuppets();

export const activePuppet = derived(
	puppets,
	($puppets) => ($puppets[PlayerType.player].find((puppet) => puppet.active) || { value: 0 }).value
);
