import { writable } from 'svelte/store';
import { announce } from '../logic/a11y.utils';

export interface SnackbarMessage {
	message: string;
	actionText?: string;
}

export interface SnackbarType {
	isVisible: boolean;
	data?: SnackbarMessage;
}

const initialSnackbar: SnackbarType = {
	isVisible: false
};

function createSnackbar() {
	const { subscribe, set, update } = writable(structuredClone(initialSnackbar));

	const hide = () => {
		announce('');
		update((snackbar) => ({ ...snackbar, isVisible: false }));
	};

	const setVisible = () => {
		update((snackbar) => {
			if (snackbar.data?.message) {
				announce(snackbar.data.message);
			}
			return { ...snackbar, isVisible: true };
		});
	};

	const show = (message: string, actionText?: string) => {
		announce(message);
		update((snackbar) => ({ ...snackbar, isVisible: true, data: { message, actionText } }));
	};

	return {
		subscribe,
		reset: () => set(structuredClone(initialSnackbar)),
		hide,
		setVisible,
		initiate: (message: string, actionText?: string) =>
			update((snackbar) => ({ ...snackbar, data: { message, actionText } })),
		show
	};
}

export const snackbar = createSnackbar();
